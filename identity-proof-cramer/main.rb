# -*- encoding: utf-8 -*-
# Autor: Gabriel Jordão Trabasso
# Trabalho de Cálculo Numérico
# Prof. Dr. Francisco Braun, UFSCar, 2013
require "./matrix_ops"

def elapsed_time(start_time, end_time)
	elapsed_time = (end_time - start_time) * 1000.0
end

puts "
Olá, meu nome é MatrixOperator, e como diz meu nome, realizo operações em matrizes! 
Irei provar para você que uma matrix quadrada multiplicada pela sua inversa resulta na matriz identidade.
No entanto, irei utilizar o método de Cramer para calcular o determinante... 
É um método não computável, e dependendo da dimensão da matriz que você fornecer, minha execução pode demorar muito tempo, e até mesmo falhar :(
"
puts

exit = false

while (!exit) 

	puts "Por favor, digite a dimensão da matriz quadrada (número inteiro estritamente positivo):"
	dimension = gets.chomp.to_i

	while (dimension <= 0)
		puts "Hmmm.. eu quis dizer um número maior que zero!"	
		dimension = gets.chomp.to_i
	end

	if (dimension < 5)
		puts "Matriz #{dimension}x#{dimension}? Isso é fácil!"
	elsif (dimension < 9)
		puts "Matriz #{dimension}x#{dimension}, eu dou conta."
	else
		puts "Matriz #{dimension}x#{dimension}!? Não me responsabilizo se não conseguir calcular, eu avisei que o método não é computável!"
	end	

	matrix_ops = MatrixOps::MatrixOperator.new
	start_time = Time.new
	m = Matrix.build(dimension) { rand(-10.0..10.0)	}
	elapsed_time = elapsed_time(start_time, Time.new)
	puts
	puts "Matriz gerada:"
	matrix_ops.print_matrix(m)
	puts "	Tempo decorrido: #{elapsed_time}ms."
	puts 

	puts "Cálculo do Determinante:"
	start_time = Time.new
	det = matrix_ops.determinant_value(m)
	elapsed_time = elapsed_time(start_time, Time.new)
	if (det != 0.0)
		puts det.to_s
		puts "	Tempo decorrido: #{elapsed_time}ms."
		puts
		
		puts "Matriz inversa:"
		start_time = Time.new
		inverse_m = matrix_ops.inverse_matrix(m)
		elapsed_time = elapsed_time(start_time, Time.new)
		matrix_ops.print_matrix(inverse_m)
		puts "	Tempo decorrido: #{elapsed_time}ms."
		puts
		
		puts "Matriz X Matriz Inversa = Identidade:"
		start_time = Time.new
		identity_m = m * inverse_m
		elapsed_time = elapsed_time(start_time, Time.new)
		matrix_ops.print_matrix(identity_m)
		puts "	Tempo decorrido: #{elapsed_time}ms."
		puts

		puts "Consegui! Espero que os erros de aritmética de ponto flutuante não tenham gerado um resultado inválido... Irei aproximar os valores da matriz identidade para valores inteiros."
		identity_m = matrix_ops.to_integer_matrix(identity_m)
		matrix_ops.print_matrix(identity_m)		
	else
		puts "Que infortúnio! A matrix gerada aleatóriamente não é invertível, pois seu determinante é igual a 0!"
	end
	puts "Gostaria de executar novamente? [s]im ou [n]ão?"
	type = gets.chomp

	while !(type == 's' || type == 'n') 
		puts "Erro de digitação? Tente novamente, digite s ou n."
		type = gets.chomp
	end

	if (type == 's')
		exit = false 
	elsif (type == 'n')
		puts "Não? Que pena... Até a próxima!"
		exit = true
	end

end
