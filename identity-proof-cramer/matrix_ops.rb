# -*- encoding: utf-8 -*-
# Autor: Gabriel Jordão Trabasso
# Trabalho de Cálculo Numérico
# Prof. Dr. Francisco Braun, UFSCar, 2013
require "matrix"

# Este módulo matrix, referenciado pelo comando 'require "matrix"' contém uma estrutura de dados que implementa uma Matriz, 
#e faz parte da biblioteca Ruby.
# No entanto, criei este módulo MatrixOps para realizar operações em cima dessa estrutura. 
# Implementar a estrutura de dados do zero é trabalhoso, e acredito que não seja o objetivo deste trabalho.
# Como era de se esperar, neste módulo não existe método para calcular determinante usando regra de Cramer, já que não é computável,
#e também não possui método para calcular matriz de cofatores. 
# O módulo contém muitos outros métodos interessantes, inclusive a fatoração LU, e pode ser visualizado neste site:
# www.ruby-doc.org/stdlib-2.0.0/libdoc/matrix/rdoc/Matrix.html

module MatrixOps

	class MatrixOperator

		def initialize()			
		end	

		# Matriz de cofatores de uma matriz M. Para cada elemento de M, é calculado o seu cofator e armazenado em uma lista.
		# Após o cálculo de todos os cofatores, a matriz de cofatores é construída a partir dos elementos da lista.		
		# Para matriz quadrada de dimensão 1, retorna-se uma matriz [1], validando o cálculo de matriz inversa para este caso.
		def cofactor_matrix(m)
			if (m.row_size > 1)
				cofactors = Array.new
				m.each_with_index do |e, row, col|
					cofactors << cofactor(m, row, col)
				end
				cofactor_m = Matrix.build(m.row_size) { cofactors.shift }
			else
				cofactor_m = Matrix.build(1) { 1 }
			end
		end

		# Cálculo do cofator do elemento (a)ij da matriz M.
		def cofactor(m, row, col)
			(-1)**(row + col) * determinant_value(minor_matrix(m, row, col))
		end		

		# Matriz M sem a linha i e coluna j, utilizada no cálculo do cofator do elmento (a)ij.
		# Todos os elementos da matriz M são armazenados em uma lista, exceto os elementos correspondentes a linha i ou coluna j.
		# Após a verificação, uma nova matriz M' é construída a partir dos elementos da lista, com dimensão da matriz M - 1.
		def minor_matrix(m, row, col)
			if (m.row_size > 1)
				minors = Array.new 
				m.each_with_index do |e, i, j|
					if !(row == i || col == j)
						minors << e
					end
				end
				minor_m = Matrix.build(m.row_size-1) { minors.shift }
			else
				m
			end
		end

		# Cálculo do determinante de uma matriz M.
		# Para matrizes quadradas com dimensão maior ou igual a 3, o cálculo é recursivo utilizando o método de Cramer.
		def determinant_value(m)
			if (m.row_size == 1)
				m[0, 0]				
			elsif (m.row_size == 2)
				(m[0, 0] * m[1, 1]) - (m[0, 1] * m[1, 0])
			else #Regra de Cramer para cálculo de determinantes, O(n!).				
				elements = Array.new
				m.row(0).each do |e|
					elements << e
				end			
				det = 0
				elements.each_with_index do |e, j|	
					det += e * cofactor(m, 0, j)
				end
				det
			end
		end

		# Matriz transposta de uma matriz M. 
		# As linhas da Matriz M são armazenadas em uma lista, e uma nova matriz M' é construída onde as suas colunas são as linhas de M.
		def transpose_matrix(m)
			rows = m.row_vectors
			transpose_m = Matrix.columns(rows)
		end

		# Cálculo da matriz adjunta de uma matriz M.
		def adjugate_matrix(m)
			adjugate_m = transpose_matrix(cofactor_matrix(m))
		end

		# Cálculo da matriz inversa de uma matriz M, caso determinante de M seja diferente de 0.
		def inverse_matrix(m)
			det = determinant_value(m)			
			if (det != 0)
				inverse_m = (1/det) * adjugate_matrix(m)
			end
		end

		# Cálculo da matriz identidade a partir de uma matriz M e sua matriz inversa.
		def identity_matrix(m)
			identity_m = m * inverse_matrix(m)
		end

		# Conversão de uma matriz M com valores de ponto flutuante para valores inteiros.
		# Todos os elementos de M são armazenados já com seus valores arredondados em uma lista.
		# Após os arredondamentos, uma nova matriz M' é construída a partir dos elementos da lista.
		def to_integer_matrix(m)
			integers = Array.new
			m.each_with_index do |e, row, col|
				integers << e.round
			end
			integer_m = Matrix.build(m.row_size) { integers.shift }
		end

		# Impressão de uma matriz M.
		def print_matrix(m)
			puts
			m.each_with_index do |e, row, col|
				print " | #{e} | "
				if (col + 1) % m.column_size == 0
  					puts 
				end  					
			end
			puts
		end

	end	

end
